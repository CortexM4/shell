﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CubicSpline))]
public class SplineEditor : Editor {

	public void OnSceneGUI () {
        CubicSpline cs = target as CubicSpline;
        Transform handleTransform = cs.transform;
        Quaternion handleRotation = handleTransform.rotation;

		for (int i = 0; i < cs.points.Count; i++) {
            EditorGUI.BeginChangeCheck();
            Handles.Label(cs.points[i] + Vector2.up * 2,
                     cs.points[i].ToString());
            Vector2 point_tmp = Handles.PositionHandle(cs.points[i], handleRotation);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(cs, "Move Point");
                EditorUtility.SetDirty(cs);

				/*if (i == cs.points.Count-1)
					cs.MoveLastPosition(point_tmp);
				else*/
					cs.points[i] = point_tmp;
            }
        }

		cs.BuildSpline();
		Handles.color = Color.blue;
		for (float x = cs.points[0].x; x < cs.points[cs.points.Count-1].x; x++)
			Handles.DrawLine (new Vector2 (x, cs.Interpolate (x)), new Vector2 (x + 1, cs.Interpolate (x + 1)));

        Handles.BeginGUI();
		GUILayout.BeginArea(new Rect(10, 10, 100, 100));
		if(GUILayout.Button("Add point")) {
			Vector3 position = SceneView.currentDrawingSceneView.camera.transform.position;
            cs.AddPointWithSort(position);
            Debug.Log(position);
		}
        if (GUILayout.Button("Sort point")) {
            cs.SortVector2List();
            Debug.Log("Sorting");
        }
        GUILayout.EndArea();
		Handles.EndGUI();
    }
}
