﻿using UnityEngine;
using System.Collections;

public class Acceleration : MonoBehaviour {

	float speed = 50.0F;
	Rigidbody2D rb2;

	void Start() {
		rb2 = GetComponent<Rigidbody2D> ();
	}

	void Update() {
		//Vector3 direction = Vector3.zero;
		//direction.y = -Input.acceleration.x;
		//direction *= Time.deltaTime;
		//transform.Translate(direction * speed);
		Vector3 direction = Vector3.zero;
		direction.y = -Input.acceleration.x;

		/* Ысе эти множители надо выставить в соответствии с играбельностью */

		if (Mathf.Abs( direction.y ) < 0.5f)
			speed = 50.0f;
		else if (Mathf.Abs( direction.y ) >= 0.5 && Mathf.Abs( direction.y ) < 0.7)
			speed = 75.0f;
		else if (Mathf.Abs( direction.y ) >= 0.7f)
			speed = 500.0f;

		direction *= Time.deltaTime;
		rb2.AddForce(direction * speed, ForceMode2D.Impulse);
	}

	/*void FixedUpdate() {
		Vector3 direction = Vector3.zero;
		direction.y = -Input.acceleration.x;
		//direction *= Time.deltaTime;
		rb2.AddForce(direction * speed);
	}*/
}
