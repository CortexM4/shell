﻿using UnityEngine;
using System.Collections;

public class AnimatorCompass : MonoBehaviour {

	public static AnimatorCompass Instance { get; private set;}
	Animator anim;

	void Awake() {
		Instance = this;
	}
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	public void DisableCompass() {
		anim.SetBool ("visible", false);
	}
	public void EnableCompass() {
		anim.SetBool ("visible", true);
	}
}
