﻿using UnityEngine;
using System.Collections;

public class AnimatorScore : MonoBehaviour {

	public static AnimatorScore Instance { get; private set;}
	Animator anim;
	// Use this for initialization
	void Awake() {
		Instance = this;
	}
	void Start () {
		anim = GetComponent<Animator> ();
	}
	

	public void PlayAnimation(){
		anim.SetBool("scoreUp", true);
	}

	void EndAnimation() {
		anim.SetBool("scoreUp", false);
	}

}
