﻿using UnityEngine;
using System.Collections;

/*
 * Класс будет отвечать за всю анимацию в игре
 * Посмотрим, что из этого выдет
 */
public class AnimatorController : MonoBehaviour {

	public static AnimatorController Instance { get; private set;}
	Animator anim;
	// Use this for initialization
	void Awake() {
		Instance = this;
	}
	void Start () {
		anim = GetComponent<Animator> ();
	}

	/*
	 * Метод останавливает анимацию "record", т.е. перестает мигать
	 */
	public void RecordAnimaStay() {
		anim.SetBool ("stay", true);
	}

	/*
	 * Анимаци завершения уровня (ее еще нет, надо допилить)
	 */
	public void LevelCompleted() {
		anim.SetBool ("levelCompleted", true);
	}

	public void DisableCompass() {
		anim.SetBool ("visible", true);
	}
	public void EnableCompass() {
		anim.SetBool ("visible", false);
	}

	/*я
	 * Метод переключает анимацию Idle персонажа
	 */
	public void ChangeCharacterIdleState() {
		int idle = (int)Random.Range(1.0F, 4.0F);		// Последнее не включается
		anim.SetInteger("Idle_num", idle);
	}
}
