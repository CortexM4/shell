﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum RecordAction { shellMovement }
public enum RecordStatus { stopped = 1, paused, recording }
public enum ReplayStatus { stopped = 1, paused, replaying }

public class ReplayRecorder : MonoBehaviour {

	private List<RecordedEvent> replayData = new List<RecordedEvent> ();
	private float startedRecordin = 0;
	private RecordStatus recordStatus;
	private ReplayStatus replayStatus;
	private bool directionReverse;

	public static ReplayRecorder Instance { get; private set;}
	// Use this for initialization
	void Awake() {
		Instance = this;
		directionReverse = true;
	}

	public void StartRecording() {	
		startedRecordin = Time.time;
		replayData = new List<RecordedEvent> ();
		recordStatus = RecordStatus.recording;
	}

	public void StartReplaying() {
		replayStatus = ReplayStatus.replaying;
	}

	public void StopRecording() {
		recordStatus = RecordStatus.stopped;
	}

	public bool IsRecording() {
		return recordStatus == RecordStatus.recording;
	}

	public void AddAction(RecordAction action, Vector3 position, Vector2 velocity) {
		if (!IsRecording ())
			return;

		RecordedEvent newAction = new RecordedEvent ();
		newAction.recordedAction = action;
		newAction.position = position;
		newAction.velocity = velocity;
		newAction.time = Time.time - startedRecordin;
		replayData.Add (newAction);
	}

	public List<RecordedEvent> GetEventsList() {
		return replayData;
	}

	public RecordedEvent GetNextElement(RecordedEvent curAction) {
		if (curAction == null) {
			return replayData[0];
		}
		else {
			RecordAction actionType = curAction.recordedAction;
			bool foundCurrent = false;
			foreach (RecordedEvent action in replayData) {
				if(foundCurrent) {
					if(actionType == action.recordedAction) {
						return action;
					}
				}
				if(action == curAction)
					foundCurrent = true;
			}
			replayStatus = ReplayStatus.stopped;
			return null;
		}
	}

	public void ReverseRecordedData(){
		replayData.Reverse();
		directionReverse = !directionReverse;
	}

	public bool GetDataDirection() {
		return directionReverse;
	}

	public bool IsReplaying() {
		return replayStatus == ReplayStatus.replaying;
	}

}

public class RecordedEvent {
	public RecordAction recordedAction;
	public float time;
	public Vector3 position;
	public Vector2 velocity;
}
