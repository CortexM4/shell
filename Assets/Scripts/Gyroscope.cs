﻿using UnityEngine;
using System.Collections;

public class Gyroscope : MonoBehaviour {

	Rigidbody2D rb2;
	// Use this for initialization
	void Start () {
		rb2 = GetComponent<Rigidbody2D>();
		Input.gyro.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.gyro.enabled) {
			/*print ("x:"+Input.gyro.attitude.x + " y:"+Input.gyro.attitude.y + " z:"+Input.gyro.attitude.z +
			       " w:" +Input.gyro.attitude.w);*/
			Vector2 gyro = new Vector2 (0, Input.gyro.rotationRate.z);
			rb2.AddForce( gyro*50);
		}
	}
}
