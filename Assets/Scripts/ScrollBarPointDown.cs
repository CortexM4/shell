﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ScrollBarPointDown : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public void OnPointerDown (PointerEventData eventData) 
	{
		AnimatorCompass.Instance.EnableCompass ();
		GunManager.Instance.MouseDown();
		CubicSpline.Instance.DrawLine (); 
	}

	public void OnPointerUp (PointerEventData eventData) 
	{
		AnimatorCompass.Instance.DisableCompass ();
		GunManager.Instance.Fire();
	}
}
