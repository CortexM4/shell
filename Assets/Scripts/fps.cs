﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class fps : MonoBehaviour {

	public Text fpsText;
	public float updateInterval = 0.5F;
	private double lastInterval;
	private int frames = 0;
	private float fps_value;
	void Start() {
		lastInterval = Time.realtimeSinceStartup;
		frames = 0;	
	}
	void Update() {
		++frames;	
		float timeNow = Time.realtimeSinceStartup;
		if (timeNow > lastInterval + updateInterval) {
			fps_value = (float) (frames / (timeNow - lastInterval));
			frames = 0;
			lastInterval = timeNow;
			fpsText.text = "FPS: " + fps_value.ToString("f2");
		}
	}
}
