﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]

public class ShaderEffect : MonoBehaviour 
{
	// Noise grain takes random intensity from Min to Max.
	[Range(0.0f,5.0f)]
	public float grainIntensity = 0.26f;
	[Range(0.0f, 5.0f)]
	public float scratchIntensity = 0.8f;
	/// The size of the noise grains (1 = one pixel).
	[Range(0.1f, 50.0f)]
	public float grainSize = 2.0f;

	public static ShaderEffect Instance { get; private set;}
	public Shader shaderNoise;
	public Texture grainTexture;
	public Texture scratchTexture;
	private Material _material;
	private bool enable;
	private int _scratchEffect;
		
	void Awake() {
		Instance = this;
		_scratchEffect = 0;
		enable = false;
	}

	public void EnableEffect(bool state) {
		enable = state;
	}

	public void EnableScratch(bool state){
		if (state)
			_scratchEffect = 1;
		else
			_scratchEffect = 0;
	}

	protected Material material
	{
		get
		{
			if (_material == null)
			{
				_material = new Material(shaderNoise);
				_material.hideFlags = HideFlags.HideAndDontSave;
			}
			return _material;
		}
	}

	private void SanitizeParameters()
	{
		/*grainIntensityMin = Mathf.Clamp( grainIntensityMin, 0.0f, 5.0f );
		grainIntensityMax = Mathf.Clamp( grainIntensityMax, 0.0f, 5.0f );
		scratchIntensityMin = Mathf.Clamp( scratchIntensityMin, 0.0f, 5.0f );
		scratchIntensityMax = Mathf.Clamp( scratchIntensityMax, 0.0f, 5.0f );
		scratchFPS = Mathf.Clamp( scratchFPS, 1, 30 );
		scratchJitter = Mathf.Clamp( scratchJitter, 0.0f, 1.0f );*/
		grainSize = Mathf.Clamp( grainSize, 0.1f, 50.0f );
	}
	
	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		if(!enable) {
			Graphics.Blit(source, destination);
			return;
		}
		SanitizeParameters ();
		if (shaderNoise == null) return;
		Material mat = material;
		mat.SetTexture("_GrainTex", grainTexture);
		mat.SetTexture("_ScratchTex", scratchTexture);
		mat.SetVector("_GrainOffsetScale", new Vector4(
			Random.value,
			Random.value,
			0, 0
			));
		mat.SetVector("_ScratchOffsetScale", new Vector4(
			0,
			Random.value,
			0, 0
			));
		mat.SetInt ("scratchEffect", _scratchEffect);
		mat.SetVector("_Intensity", new Vector4(	
			grainIntensity,
			scratchIntensity,	
			0, 0 ));

		Graphics.Blit(source, destination, mat);
	}
	
	void OnDisable()
	{
		if (_material)
		{
			DestroyImmediate(_material);
		}
	}
}