﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class ShaderHeartBeat : MonoBehaviour {

	//private AudioSource audioSource;
	public Shader shader;

	private Material _material;
	private float _spectrum;
	// Use this for initialization
	void Start () {
		_spectrum = 0.0F;
		//audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		//if (audioSource.isPlaying) {
		//	float[] spectrum = audioSource.GetSpectrumData(64, 0, FFTWindow.Rectangular);
		//	_spectrum = spectrum [0];
		//}
		//else
		//	_spectrum = 0.0F;
		//transform.position = new Vector3 (0, spectrum [0]);
		if(TimeManager.Instance != null)
			_spectrum = TimeManager.Instance.getShaderValue ();
	}

	protected Material material
	{
		get
		{
			if (_material == null)
			{
				_material = new Material(shader);
				_material.hideFlags = HideFlags.HideAndDontSave;
			}
			return _material;
		}
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		if (shader == null) return;
		Material mat = material;
		float oneOverBaseSize = 80.0f / 512.0f; // to keep values more like in the old version of fisheye
		
		float ar = (source.width * 1.0f) / (source.height * 1.0f);

		mat.SetFloat("_frequency", _spectrum * ar * oneOverBaseSize);

		Graphics.Blit(source, destination, mat);
	}

	void OnDisable()
	{
		if (_material)
		{
			DestroyImmediate(_material);
		}
	}
}
