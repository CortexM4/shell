﻿using UnityEngine;
using System.Collections.Generic;


//[ExecuteInEditMode]
public class CubicSpline : MonoBehaviour {

	public static CubicSpline Instance { get; private set;}

    public List<Vector2> points = new List<Vector2>();
	private static SplineTuple[] splines; // Сплайн

	private LineRenderer lineRender;
	private int count;
	private Color color;

	// Ширина экрана
	private float width;
	// Тот отрезок, который будет отображать кирвую относительно центра
	private float span;

	// Структура, описывающая сплайн на каждом сегменте сетки
	private struct SplineTuple
	{
		public float a, b, c, d, x;
	}

	void Awake() {
		Instance = this;
		width = Camera.main.pixelWidth;
		span = width / 3;
	}


	void Start(){
		BuildSpline ();
		count = (int)(points[points.Count-1].x - points[0].x);
		lineRender = GetComponent<LineRenderer> ();
		//material = lineRender.material;
		lineRender.SetWidth (0.1F, 0.1F);
		lineRender.SetVertexCount(count);
		//color = Color.blue;
		//lineRender.SetColors(color, color);
	}

	/*
	 *  Создание сплайнов и прорисовка кривой
	 */
	public void DrawLine() {
		BuildSpline ();
		//lineRender.enabled = true;
		Vector3 lSpan = Camera.main.ScreenToWorldPoint(new Vector3((width/2-span), Camera.main.transform.position.y, Camera.main.transform.position.z));
		Vector3 rSpan = Camera.main.ScreenToWorldPoint(new Vector3((width/2), Camera.main.transform.position.y, Camera.main.transform.position.z));
		float leftSpan = lSpan.x;
		float rightSpan = rSpan.x;
		//count = (int)(points[points.Count-1].x - points[0].x);
		count = (int)((leftSpan - rightSpan)/0.1f);
		lineRender.SetVertexCount(count);
		for (int i = 0; i < count; i ++) {
			if (rightSpan < splines[points.Count - 1].x) // Рисуем только те элементы, которые находятся в интервале
				lineRender.SetPosition (i, new Vector2(rightSpan, Interpolate (rightSpan)));
			rightSpan += 0.1f;
		}
	}

	public void SetAlphaLineRender(float alpha) {
		Color c = Color.white;
		c.a = alpha;
		lineRender.SetColors (c, c);
	}

	/*
	 *  Отключение линии когда шар столкнулся с препятствием
	 */
	public void DisableLineRender() {
		lineRender.enabled = false;
	}

	public void EnableLineRender() {
		lineRender.enabled = true;
	}

    public void AddPointWithSort(Vector2 p)
    {
        points.Add(p);
        SortVector2List();
    }

    public void SortVector2List()
    {
        points.Sort(delegate (Vector2 p1, Vector2 p2)
        {
            return p1.x.CompareTo(p2.x);
        });
    }

	/*
	 *  Перемещение любой точки по индексу
	 */
	public void MovePoint(int index, Vector2 new_position){
		lineRender.enabled = true;
		points[index] = new_position;
		DrawLine ();
	}

	/*
	 *  Перемещение последней точки
	 *  В текущий момент цель не будет двигаться к пирату
	 *  поэтому все в коменты
	 *
	public void MoveLastPosition(Vector2 new_position) {
		int lastIndex = points.Count - 1;
		points[lastIndex] = new_position;
		if(points.Count >=3) {
			if((points[lastIndex].x - points[lastIndex-1].x) < 10){
				points.RemoveAt(lastIndex - 1);
			}
		}
	}
	*/

	// Построение сплайна
	// x - узлы сетки, должны быть упорядочены по возрастанию, кратные узлы запрещены
	// y - значения функции в узлах сетки
	// n - количество узлов сетки
	public void BuildSpline()
	{
		// Инициализация массива сплайнов
		int n = points.Count;
		splines = new SplineTuple[n];

		for (int i = 0; i < n; ++i)
		{
			splines[i].x = points[i].x;
			splines[i].a = points[i].y;
		}
		splines[0].c = splines[n - 1].c = 0.0F;
		
		// Решение СЛАУ относительно коэффициентов сплайнов c[i] методом прогонки для трехдиагональных матриц
		// Вычисление прогоночных коэффициентов - прямой ход метода прогонки
		float[] alpha = new float[n - 1];
		float[] beta  = new float[n - 1];
		alpha[0] = beta[0] = 0.0F;
		for (int i = 1; i < n - 1; ++i)
		{
			float hi  = points[i].x - points[i - 1].x;
			float hi1 = points[i + 1].x - points[i].x;
			float A = hi;
			float C = 2.0F * (hi + hi1);
			float B = hi1;
			float F = 6.0F * ((points[i + 1].y - points[i].y) / hi1 - (points[i].y - points[i - 1].y) / hi);
			float z = (A * alpha[i - 1] + C);
			alpha[i] = -B / z;
			beta[i] = (F - A * beta[i - 1]) / z;
		}
		
		// Нахождение решения - обратный ход метода прогонки
		for (int i = n - 2; i > 0; --i)
		{
			splines[i].c = alpha[i] * splines[i + 1].c + beta[i];
		}
		
		// По известным коэффициентам c[i] находим значения b[i] и d[i]
		for (int i = n - 1; i > 0; --i)
		{
			float hi = points[i].x - points[i - 1].x;
			splines[i].d = (splines[i].c - splines[i - 1].c) / hi;
			splines[i].b = hi * (2.0F * splines[i].c + splines[i - 1].c) / 6.0F + (points[i].y - points[i - 1].y) / hi;
		}
	}
	
	// Вычисление значения интерполированной функции в произвольной точке
	public float Interpolate(float x)
	{
		if (splines == null)
		{
			return float.NaN; // Если сплайны ещё не построены - возвращаем NaN
		}
		
		int n = splines.Length;
		SplineTuple s;
		
		if (x <= splines[0].x) // Если x меньше точки сетки x[0] - пользуемся первым эл-тов массива
		{
			s = splines[0];
		}
		else if (x >= splines[n - 1].x) // Если x больше точки сетки x[n - 1] - пользуемся последним эл-том массива
		{
			s = splines[n - 1];
		}
		else // Иначе x лежит между граничными точками сетки - производим бинарный поиск нужного эл-та массива
		{
			int i = 0;
			int j = n - 1;
			while (i + 1 < j)
			{
				int k = i + (j - i) / 2;
				if (x <= splines[k].x)
				{
					j = k;
				}
				else
				{
					i = k;
				}
			}
			s = splines[j];
		}
		
		float dx = x - s.x;
		// Вычисляем значение сплайна в заданной точке по схеме Горнера (в принципе, "умный" компилятор применил бы схему Горнера сам, но ведь не все так умны, как кажутся)
		return s.a + (s.b + (s.c / 2.0F + s.d * dx / 6.0F) * dx) * dx; 
	}
}
