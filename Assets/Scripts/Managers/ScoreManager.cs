﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ScoreManager : MonoBehaviour {

	public static ScoreManager Instance {get; private set;}
	public CubicSpline Curve;
	public Text scoreText;
	public Text scoreTextUp;

	private BallManager ball = null;
	private int index = 0;
	private float deltaPoint;
	private int numberSection = 10;
	private int countPoint;
	private float score;
	private List<float> lineScore = new List<float>();
	// Use this for initialization

	void Awake() {
		Instance = this;
		countPoint = CubicSpline.Instance.points.Count;
		deltaPoint = (CubicSpline.Instance.points[countPoint - 1].x - CubicSpline.Instance.points[0].x) / numberSection;
	}

	public void Initialize(BallManager bm) {
		ball = bm;
		lineScore.Clear();
		countPoint = CubicSpline.Instance.points.Count;
		deltaPoint = (CubicSpline.Instance.points[countPoint - 1].x - CubicSpline.Instance.points[0].x) / numberSection;
		index = 0;
		score = 0;

	}
	
	// Update is called once per frame
	void Update () {
	}

	void FixedUpdate() {
		if(ball != null) {
			if((ball.transform.position.x > CubicSpline.Instance.points[0].x+index*deltaPoint) && 
			   ball.transform.position.x <= CubicSpline.Instance.points[countPoint - 1].x &&
			   lineScore.Count == index) {
				float currentScore = Mathf.Abs( CubicSpline.Instance.Interpolate(ball.transform.position.x) - ball.transform.position.y );
				score += 100 - currentScore;
				scoreText.text = "Current Score: " + score.ToString();
				scoreTextUp.text = "+"+Mathf.FloorToInt(score).ToString();
				AnimatorScore.Instance.PlayAnimation();
				lineScore.Add(currentScore);
				index++;
			}
		}
	}
	
}
