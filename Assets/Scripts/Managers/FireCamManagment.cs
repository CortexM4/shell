﻿using UnityEngine;
using System.Collections;

public class FireCamManagment : MonoBehaviour {

	public static FireCamManagment Instance { get; private set;}

	private Vector2 m_CurrentVelocity;
	private Vector2 moveCoord;
	private Vector2 startPos;
	private float timeView;
	private bool isMoved = false;
	private Vector2 velocity = Vector2.zero;

	void Awake() {
		Instance = this;
		startPos = transform.position;
	}

	void FixedUpdate(){
		if(isMoved) {

			/*if( (moveX - transform.position.x)*direction > speedViewCam*Time.deltaTime) {
				float currentX = transform.position.x + speedViewCam * Time.deltaTime * direction;
				transform.position = new Vector3 (currentX, transform.position.y, transform.position.z);
			}
			else {
				transform.position = new Vector3 (moveX, transform.position.y, transform.position.z);
				
				isMoved= false;
			}*/
			CubicSpline.Instance.DrawLine();
			//Vector2 tmpPost = Vector2.MoveTowards(transform.position, moveCoord, 10 * 0.02F);
			Vector2 tmpPost = Vector2.SmoothDamp(transform.position, moveCoord, ref velocity, timeView);
			//Vector2 tmpPost = Vector2.Lerp(transform.position, moveCoord, 0.01F);
			//if(tmpPost.y < startPos.y)
			//	tmpPost.y = startPos.y;
			transform.position  = new Vector3 ( tmpPost.x, tmpPost.y, transform.position.z);

			/* Условие проверяющее достигли - ли мы цели */
			if( Mathf.Abs((moveCoord - new Vector2( transform.position.x, transform.position.y)).magnitude) < 1F)
				isMoved = false;
		}
	}
	
	public void GoTo(Vector2 coord) {
		Vector2 tmpCoord;
		tmpCoord.x = Mathf.Clamp(coord.x, -100 , 100);
		tmpCoord.y = Mathf.Clamp(coord.y, startPos.y, 100);
		transform.position = new Vector3(tmpCoord.x, tmpCoord.y, transform.position.z);
		CubicSpline.Instance.EnableLineRender();
		CubicSpline.Instance.DrawLine();
		isMoved = false;
	}

	public Vector2 GetStartPoint() {
		return startPos;
	}

	public void MoveToStart(float time) {
		CubicSpline.Instance.DisableLineRender();
		MoveTo(startPos, time);
	}
	public void MoveTo(Vector2 coord, float time) {
		velocity = Vector2.zero;
		timeView = time;
		/* Сразу ограничим вектор, чтобы потом этим не заниматься */
		moveCoord.x = Mathf.Clamp(coord.x, -100, 100);
		moveCoord.y = Mathf.Clamp(coord.y, startPos.y, 100);
		isMoved = true;
	}
	public bool isCameraMoved(){
		return isMoved;
	}
}
