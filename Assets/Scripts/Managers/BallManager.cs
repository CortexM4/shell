﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BallManager : MonoBehaviour {
	public GameObject explosion;
	Rigidbody2D rb2;
	float start_time;
	bool PositionFlag;					// флаг для старта записи
	
	ReplayStatus replayStatus;
	RecordedEvent action;

	///private float maxLengthForMove = 20.0F;			// Если в радиусе этого расстояния до цели прилетел ball, то aim будет двигаться к ГГ
	private float maxLength = 40.0F;				// Та дистанция до цели, по достижению которой сохраняется position и velosity
	float delayReplay = 1.0F;				// Время, после которого начнется отмотка назад
	void Start () {
		rb2 = GetComponent<Rigidbody2D>();
		PositionFlag = true;
		start_time = Time.time;

		Time.timeScale = 1.0F;
		HUDManager.Instance.DisableScrollBarGun();
		GunManager.Instance.setFired(true);
		ScoreManager.Instance.Initialize(this);
	}
	
	// Update is called once per frame
	void Update () {
		if (ReplayRecorder.Instance.IsRecording ()) {
			ReplayRecorder.Instance.AddAction (RecordAction.shellMovement, transform.position, rb2.velocity);
		}

		FireCamManagment.Instance.GoTo(transform.position);

	}

	void FixedUpdate() {
		if (Mathf.Abs ((AimController.Instance.GetPositionToWorldPoint() - transform.position).magnitude) <= maxLength && PositionFlag) {
			ReplayRecorder.Instance.StartRecording();
			PositionFlag = false;
		}

		if (ReplayRecorder.Instance.IsReplaying ()) {
			action = ReplayRecorder.Instance.GetNextElement(action);
			if(action != null) {
				transform.position = action.position;

				if(ReplayRecorder.Instance.GetDataDirection())
					rb2.velocity = action.velocity;

			}
		}

		// Это полная херня (Удаление должно быть по триггеру)
		if (transform.position.x > 100 || transform.position.x < -100) {
			HUDManager.Instance.EnableScrollBarGun();
			GunManager.Instance.setFired(false);
			FireCamManagment.Instance.MoveToStart(50.0F);
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		CubicSpline.Instance.DisableLineRender ();
		if (other.tag == "Aim") {
			if(!AimController.Instance.IsAimDestroed()) {			// Это вообще отстой, функция срабатывала дважды, вызывая ошибки
				AimController.Instance.AimDestroed();				// этот флаг выставляется дабы избежать двойного попадания сюда
				rb2.isKinematic = true;								// и да если проверять в триггер цели, то в это условие не попадем
				rb2.velocity = Vector2.zero;
				ReplayRecorder.Instance.StopRecording();;
				StartCoroutine(RunReplay());
			}

		} else if (other.tag == "GroundSand") {

			/*
			 * Так, в связи с изменением концепции, движение цели к объекту отменяется
			 * если что, просто раскоментить
			Vector2 tmp_pos = transform.position - AimController.Instance.GetPositionToWorldPoint();
			if( Mathf.Abs (tmp_pos.magnitude ) < maxLengthForMove) {	// Снаряд разоравлся рядом с целью
				AimController.Instance.MoveToCharacter();				// то начинаем двигаться к цели
			}
			*/

			Instantiate(explosion, transform.position, transform.rotation);
			Destroy(gameObject);
		}


	}

	IEnumerator RunReplay() {

		CubicSpline.Instance.DisableLineRender ();
		HUDManager.Instance.EnableReplayPanel ();					// Включить панель типа "камера"
		ShaderEffect.Instance.EnableEffect(true);					// Включение шедера эффектов
		yield return new WaitForSeconds (delayReplay);
		AnimatorController.Instance.RecordAnimaStay ();				// Остановить мерцание rec image (переключаем на stay анимацию)
		Time.timeScale = 2.0F;
		ShaderEffect.Instance.EnableScratch (true);					// Включаем полосы перемотки а шейдере
		ReplayRecorder.Instance.ReverseRecordedData();				// сохраненные данные (replay) реверсим
		ReplayRecorder.Instance.StartReplaying();					// Запускаем повтор (см. FixedUpdate)
		while (ReplayRecorder.Instance.IsReplaying())
			yield return null;

		ShaderEffect.Instance.EnableScratch (false);				// Тут момент начала записи (выключаем полосы для медленного воспроизвениея, но с эффектом страения пленки)
		yield return new WaitForSeconds (0.2F);
			

		Time.timeScale = 0.3F;

		ReplayRecorder.Instance.ReverseRecordedData();
		ReplayRecorder.Instance.StartReplaying();
		while (ReplayRecorder.Instance.IsReplaying())

			yield return null;

		ShaderEffect.Instance.EnableEffect(false);
		HUDManager.Instance.DisableRepalyPanel();
		Time.timeScale = 1.0F;
		yield return new WaitForSeconds (0.1F);
		Destroy (gameObject);
		GameController.Instance.LevelCompleted();					// Все, уровень пройден
	}
}
