﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDManager : MonoBehaviour {

	public static HUDManager Instance { get; private set;}

	public Transform gun;
	public Text scoreText;

	public GameObject mainPanel;
	public GameObject replayPanel;

	public Scrollbar scrollBar;
	public Text destination;

	Vector2 length;

	void Awake () {
		Instance = this;
		length = AimController.Instance.GetPositionToWorldPoint() - gun.position;

	}

	public void DisableMainPanel() {
		mainPanel.SetActive (false);
	}

	public void EnableMainPanel() {
		mainPanel.SetActive (true);
	}

	// Включает/отключает scrollBar наведения
	public void EnableScrollBarGun(){
		scrollBar.gameObject.SetActive(true);
	}
	public void DisableScrollBarGun(){
		scrollBar.gameObject.SetActive(false);
	}

	public void EnableReplayPanel(){
		mainPanel.SetActive (false);
		replayPanel.SetActive (true);
	}

	public void DisableRepalyPanel(){
		replayPanel.SetActive (false);
		mainPanel.SetActive (true);
	}

	// Update is called once per frame
	void Update () {
		/*
		 *  Изменение положнения стрелки указывающей на цель и расстояние до нее
		 */
		Vector3 coord = AimController.Instance.GetPositionToScreenPoint ();
		destination.text = Mathf.FloorToInt(AimController.Instance.GetDistanceForCharacter()).ToString() + " m.";
	}
}
