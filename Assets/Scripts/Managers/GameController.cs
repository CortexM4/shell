﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

	public Text statusText;
	public string levelName;
	public static GameController Instance {get; private set;}
	private bool isFirstStep = true;

	void Awake() {
		Instance = this;
		if(levelName == "")
			print("Name of next level is NULL");
	}

	// Use this for initialization
	void Start () {
		StartCoroutine(LevelLoaded());
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		/*
		 *  Проверяет если было нажатие на экран, то камера возвращается в первоначальное состояние
		 *  на самом деле это фигня. Пиренести эту функцию в управление камерой.
		 */
		if (Input.GetMouseButtonDown (0) && isFirstStep) {
			StopCoroutine(LevelLoaded());
			FireCamManagment.Instance.GoTo(FireCamManagment.Instance.GetStartPoint());
			HUDManager.Instance.EnableMainPanel ();
			isFirstStep = false;
		}
	}

	/*
	 *  Надо будет перенести управление GUI в HUDManager
	 *  и если будет анимация, то еще и в AnimatorController
	 */
	public void GameOver() {
		//AnimatorController.Instance.LevelCompleted();	// Анимация завершения уровня	
		statusText.text = "GAME OVER";
		statusText.gameObject.SetActive (true);
		StartCoroutine (Sleep (Application.loadedLevelName));
	}
	public void LevelCompleted() {
		statusText.text = "LEVEL COMPLETED";
		statusText.gameObject.SetActive (true);
		StartCoroutine (Sleep (levelName));
	}

	IEnumerator LevelLoaded() {
		HUDManager.Instance.DisableMainPanel ();
		FireCamManagment.Instance.MoveTo(AimController.Instance.GetPositionToWorldPoint(), 2);
		while(FireCamManagment.Instance.isCameraMoved())
			yield return new WaitForSeconds (0.2F);
		FireCamManagment.Instance.MoveToStart(0.5F);
		while(FireCamManagment.Instance.isCameraMoved())
			yield return new WaitForSeconds (0.2F);
		HUDManager.Instance.EnableMainPanel ();
		isFirstStep = false;
	}

	IEnumerator Sleep(string lvlName) {
		yield return new WaitForSeconds(4.0F);
		Application.LoadLevel (lvlName);
	}

}
