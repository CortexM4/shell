﻿using UnityEngine;
using System.Collections;

public class AimController : MonoBehaviour {

	public static AimController Instance { get; private set;}
	
	private bool aimDestroed;
	///private bool isMoved = false;
	///private float speed = 5.0F;
	void Awake() {
		Instance = this;	
		aimDestroed = false;
	}

	/*
	 * Нужно для движения цели к пирату
	 * в случае необходимости всю эту лабуду раскоментировать
	 * 
	void Update(){

		if(isMoved) {
			CubicSpline.Instance.MoveLastPosition (transform.position);	// Изменение положения последней точик кривой
			if( GetDistanceForCharacter() <= 0.0F) {
				isMoved = false;
				GameController.Instance.GameOver();
			}
			else
				transform.position += Vector3.left * Time.deltaTime * speed;
		}
	}

	public void MoveToCharacter(){
		isMoved = true;
	}

	void OnTriggerEnter2D(Collider2D other) {
		//aimDestroed = true;
		isMoved = false;
	}

	*/

	public float GetDistanceForCharacter() {
		return AimController.Instance.GetPositionToWorldPoint().x - GunManager.Instance.GetPosition().x;
	}

	public Vector3 GetPositionToWorldPoint(){
		return transform.position;
	}

	public Vector3 GetPositionToScreenPoint() {
		return Camera.main.WorldToScreenPoint(transform.position);
	}

	public bool IsAimDestroed() {
		return aimDestroed;
	}
	public void AimDestroed() {
		aimDestroed = true;
	}	
}
