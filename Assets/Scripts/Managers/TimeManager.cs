﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeManager : MonoBehaviour {

	public static TimeManager Instance;
	public Slider sliderTime;
	public Transform aim;
	
	private bool startSlowTime;
	private bool backToNormalState;
	private float slowTimeDuration = 1.0F;
	private float speedScale = 2.0F;
	private float speedValue = 0.015F;
	private float shaderValue = 0.0F;

	void Awake() {
		Instance = this;
	}

	void Start() {

		SplashScreen.Hide();					// Возможно это стоит перенести в какой-нибудь GameController (когда он появится)

		startSlowTime = false;
		backToNormalState = false;
		sliderTime.minValue = 0;
		sliderTime.maxValue = slowTimeDuration;
		sliderTime.value = slowTimeDuration;
	}
	/*
	 * Костыли и велосипеды.
	 * Смысл в чем, когда происходит нажатие на кнопу устанавливается флаг startSlowTime
	 * Первый этап, timeScale должен упасть до определенного уровня и в таком состоянии находится положенное время
	 * далее, по истечении, этого времени высавляется влаг backToNormalState, что сигнализирует о том, что давай-ка
	 * timeScale возвращайся в свою единицу. После того как timeScale встал в единицу startSlowTime сбрсывается,
	 * сообщая, что все вернулось в прежний режим. Т.е. эта конструкция актуальна при нажатии именно кнопки, а не изменении времени.
	 * ShaderValue отвечает за изменение шейдера "Рыбий Глаз"
	 */
	void FixedUpdate() {
		if (!startSlowTime) {
			backToNormalState = false;
			if(sliderTime.value < sliderTime.maxValue)
				sliderTime.value += Time.deltaTime/10;
		}
		if(startSlowTime && !backToNormalState) {
			sliderTime.value -= Time.deltaTime;
			if(Time.timeScale > 0.5F) {
				Time.timeScale -= speedScale*Time.deltaTime;
				shaderValue += speedValue;
			}
		}
		if(startSlowTime && backToNormalState) {
			if(Time.timeScale < 1.0F) {
				Time.timeScale += speedScale*Time.deltaTime;
				shaderValue -= speedValue;
			}
			else
				startSlowTime = false;
		}
	}

	public float getShaderValue() {
		return shaderValue;
	}

	public void TimeBack(GameObject obj) {
		Rigidbody2D rb2 = obj.GetComponent<Rigidbody2D>();
		//obj.transform.position = startPosition;
		//rb2.velocity = new Vector2(-startVelocity.x, startVelocity.y);
		rb2.velocity = new Vector2(-rb2.velocity.x, rb2.velocity.y);
		print(rb2.velocity);
		//Time.timeScale = 0.25F;
	}

	public void EnterSlowTime() {
		//Time.timeScale = 0.5F;
		if(GunManager.Instance.isFired()) {
			startSlowTime = true;
			StartCoroutine(SlowTimerCoroutine());
		}
	}

	IEnumerator SlowTimerCoroutine() {
		yield return new WaitForSeconds(sliderTime.value);
		//Time.timeScale = 1.0F;
		backToNormalState = true;
	}
}
