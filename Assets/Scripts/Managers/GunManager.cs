﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GunManager : MonoBehaviour {

	public static GunManager Instance { get; private set;}
	public BallManager instanceBall;
	public Scrollbar scrollBar;
	public Rigidbody2D ball;
	public GameObject gun_sprite;
	public GameObject body_sprite;
	public GameObject head_sprite;
	public GameObject hend_sprite;
	public GameObject arror_sprite;

	private bool fired_state;
	private float speedArror = 5.0F;
	private float speedGun = 5.0F;
	float force = 2000f;
	GameObject arror;

	float angle;
	bool isMousePressed;
	Vector2 position;
	float delta = 2.5f;			// отступ срелки наведения от спрайта пушки
	float delta_point = 10.0F;	// отступ 2-ой точки от точки вращения
	private Color arrorColor;

	void Awake() {
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		angle = 0.0f;
		Vector3 theScale = transform.localScale;
		transform.localScale = theScale;	
		isMousePressed = false;
		fired_state = false;
		//pivot = transform.parent.gameObject;
		position = gun_sprite.transform.position;		// Угол от точки вращения

		//arror = transform.parent.Find ("Arrors").gameObject;
	}

	/*
	 * Выстрел и прицеливание ведется мышью
	 */

	// Update is called once per frame
	void Update () {

		if(isMousePressed)
		{
			float angle_tmp = 45.0F * scrollBar.value;	
			Vector2 pow = Vector2.right.Rotate(angle);
			Vector2 point_tmp_pos = position+pow*delta_point;
			Vector2 point_index = Vector2.Lerp(CubicSpline.Instance.points[1], point_tmp_pos, speedArror * Time.deltaTime);
			CubicSpline.Instance.MovePoint(1, point_index);	// Перемещение второй точки и перересовка
			/*
			 *  Установка альфы lineRender'а в соответствии с arror
			 */
			arrorColor = arror_sprite.GetComponent<SpriteRenderer>().color;
			CubicSpline.Instance.SetAlphaLineRender(arrorColor.a);

			angle = angle_tmp;
			gun_sprite.transform.rotation = Quaternion.Slerp(gun_sprite.transform.rotation,
			                                                 Quaternion.Euler(0,0, angle_tmp),
			                                                 speedGun * Time.deltaTime);
			body_sprite.transform.rotation = Quaternion.Slerp(body_sprite.transform.rotation,
			                                                  Quaternion.Euler(0,0, angle_tmp/5),
			                                                  speedGun * Time.deltaTime);
			head_sprite.transform.rotation = Quaternion.Slerp(head_sprite.transform.rotation,
			                                                  Quaternion.Euler(0,0, angle_tmp/2),
			                                                  speedGun * Time.deltaTime);
			hend_sprite.transform.rotation = Quaternion.Slerp(hend_sprite.transform.rotation,
			                                                  Quaternion.Euler(0,0, angle_tmp/3),
			                                                  speedGun * Time.deltaTime);
			arror_sprite.transform.rotation = Quaternion.Slerp(arror_sprite.transform.rotation,
			                                                   Quaternion.Euler(0,0, angle_tmp),
			                                                   speedArror * Time.deltaTime);
		}
		
	}
	public void MouseDown() {
		isMousePressed = true;
	}

	/*
	 * Это состояние необходимо для:
	 *  - TimeManager (определяет, возможность перехода в SlowTime)
	 */
	public bool isFired() {
		return fired_state;
	}

	public void setFired(bool state) {
		fired_state = state;
	}

	public Vector3 GetPosition() {
		return transform.position;
	}

	public void Fire() {

		Vector2 pow = Vector2.right.Rotate(angle);
		Vector2 ball_tmp_pos = position+pow*delta;
		Vector3 ball_pos = new Vector3(ball_tmp_pos.x, ball_tmp_pos.y, transform.position.z);
		Rigidbody2D clone = Instantiate(ball, ball_pos, 
		                                Quaternion.identity) as Rigidbody2D;
		//if (player) {
		//	clone.GetComponent<BallManager> ().fireCam = Camera.main;
		//}
		clone.AddForce( pow * force );
		//anim.SetBool ("fire", false);
		//clone.AddRelativeForce( vTrack * force );
		isMousePressed = false;
		}

	void OnMouseDown()
	{
		//arror.SetActive (true);
		//isMousePressed = true;
	}
}