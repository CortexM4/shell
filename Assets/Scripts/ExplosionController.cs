﻿using UnityEngine;
using System.Collections;

public class ExplosionController : MonoBehaviour {

	ParticleSystem ps;
	// Use this for initialization
	void Start () {
		ps = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!ps.IsAlive()){
			//float deltaTime = Time.time - start_time;
			//HUDManager.Instance.UpdateScore(transform.position, deltaTime);
			HUDManager.Instance.EnableScrollBarGun();
			GunManager.Instance.setFired(false);
			FireCamManagment.Instance.MoveToStart(0.5F);
			Destroy(gameObject);
		}
	}
}
