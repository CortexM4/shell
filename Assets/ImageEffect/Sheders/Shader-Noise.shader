﻿Shader "Custom/SolidColor" {
	Properties {
        _MainTex ("Base (RGB)", 2D) = "white" { }
        _GrainTex ("Base (RGB)", 2D) = "grey" { }
        _ScratchTex ("Base (RGB)", 2D) = "gray" {}
    }
    SubShader {
        Pass {
        
        	ZTest Always Cull Off ZWrite Off Fog { Mode off }
        	
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            
            struct VS_INPUT
            {
            	float4 Position : POSITION;
            	float2 uv : TEXCOORD0;
            	float2 grain : TEXCOORD1;
            	float2 scratch : TEXCOORD2;
            };
            
            struct VS_OUTPUT
            {
            	float4 Position : POSITION;
            	float2 uv : TEXCOORD0;
            	float2 grain : TEXCOORD1;
            	float2 scratch : TEXCOORD2;
            };
            
            float2 _GrainOffsetScale;
            float2 _ScratchOffsetScale;
            int	scratchEffect;
            
            uniform sampler2D _MainTex;
            uniform sampler2D _GrainTex;
            uniform sampler2D _ScratchTex;
            
            uniform fixed4 _Intensity; // x=grain, y=scratch

            VS_OUTPUT vert(VS_INPUT Input) {
            	VS_OUTPUT Output;
                Output.Position = mul (UNITY_MATRIX_MVP, Input.Position);
                Output.uv = Input.uv;
                Output.grain = Input.grain.xy + _GrainOffsetScale.xy;
                Output.scratch = Input.scratch.xy + _ScratchOffsetScale.xy;	
                return Output;
            }

            fixed4 frag(VS_OUTPUT Input) : COLOR {
                float4 col = tex2D(_MainTex, Input.uv);
                float3 grain = tex2D(_GrainTex, Input.grain).rgb * 2 - 1;
                col.rgb += grain * _Intensity.x;
                if(scratchEffect) {
                	float3 scratch = tex2D(_ScratchTex, Input.scratch).rgb * 2 - 1;
                
                	col.rgb += scratch * _Intensity.y;
                }
                return col;
            }

            ENDCG
        }
    }
}