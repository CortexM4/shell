﻿Shader "Custom/HeartBeat" {
	Properties {
        _MainTex ("Base (RGB)", 2D) = "white" { }
    }
    SubShader {
        Pass {
        
        	ZTest Always Cull Off ZWrite Off Fog { Mode off }
        
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            struct VS_INPUT
            {
            	float4 pos : POSITION;
            	float2 uv : TEXCOORD0;
            };
            
            struct VS_OUTPUT
            {
            	float4 pos : SV_POSITION;
            	float2 uv : TEXCOORD0;
            	//float4 sPos : TEXCOORD1;			// Screen position
            };
            
            
            uniform sampler2D _MainTex;
            uniform float _frequency;

            VS_OUTPUT vert(VS_INPUT Input) {
            	VS_OUTPUT Output;
                Output.pos = mul (UNITY_MATRIX_MVP, Input.pos);
                Output.uv = Input.uv;
                //Output.sPos = ComputeScreenPos(Input.pos);
                return Output;
            }

            fixed4 frag(VS_OUTPUT Input) : COLOR {
            	//float wave = 0.02;
            	//float xPos = _ScreenParams.x / 2;
            	//float yPos = _ScreenParams.y / 2;
            	//float2 coord = (Input.sPos.xy *_ScreenParams.xy / Input.sPos.w);
            	//float2 beat = Input.uv;
            	//if(_Frequency > 0) {
            			//beat.x = Input.uv.x + wave * sin(_Frequency + radians(Input.uv.x * 360.0F));
            			//beat.y = Input.uv.y + wave * sin(_Frequency + radians(Input.uv.y * 360.0F));
            	//if(coord.y < 20 || coord.y > _ScreenParams.y - 240) { 	
            	//	beat.x = Input.uv.x + wave * sin(_Frequency + radians(Input.uv.x * 360.0F));
            	//	beat.y = Input.uv.y + wave * sin(_Frequency + radians(Input.uv.y * 360.0F));
            	//}
            	//}
            	half2 coords = Input.uv;
				coords = (coords - 0.5) * 2.0;		
		
				half2 realCoordOffs;
				realCoordOffs.x = (1-coords.y * coords.y) * _frequency * (coords.x); 
				realCoordOffs.y = (1-coords.x * coords.x) * _frequency * (coords.y);
		
				half4 color = tex2D (_MainTex, Input.uv - realCoordOffs);	 
		
				return color;
            	
                //float4 col = tex2D(_MainTex, beat);
                //if(_Frequency > 0) {
                //	if((pow(xPos, 2) + pow(yPos, 2)) > pow(50, 2)) { 	
            	//		col.rgb = float3(1, 1, 0);
            	//	}
            	//}
            	//if(coord.y < 20 || coord.y > _ScreenParams.y - 240) { 	
            	//	col.rgb = float3(1, 1, 0);
            	//}
                //return col;
            }

            ENDCG
        }
    }
}